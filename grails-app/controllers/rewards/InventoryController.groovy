package rewards

class InventoryController {

    def index() {
        render "Here is a list of everything"
    }

    def edit() {
        def productName = "Breakfast Blend"
        def sku = "BB01"

        [product:productName, sku:sku]
    }

    def remove() {
        render "You have nothing to see here"
    }

    def list() {

        def allProducts = Product.list()
        [allProducts:allProducts]

    }
}
