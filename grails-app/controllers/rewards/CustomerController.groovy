package rewards

class CustomerController {
    static scaffold = Customer

    def calculationsService

    def lookup() {
        def customer = Customer.list()
        [customerList: customer]
    }

    def customerLookup(Customer lookupInstance) {
        def (customer, welcomeMessage) = calculationsService.processCheckin(lookupInstance)
        render(view: "checkin", model: [customer: customer, welcomeMessage: welcomeMessage])
    }

    def checkin() {}

    def index() {
        params.max = 10
        [customerList: Customer.list(params), customerCount: Customer.count()]
    }

    def create() {
        [customer: new Customer()]
    }

    def save(Customer customer) {
        customer.save()
        redirect(action: "show", id: customer.id)
    }

    def show(Long id) {
        def customer = Customer.get(id)
        customer = calculationsService.getTotalPoints(customer)
        [customer: customer]
    }


    def edit(Long id) {
        def customer = Customer.get(id)
        [customer: customer]
    }

    def update(Long id) {
        def customer = Customer.get(id)
        customer.properties = params
        customer.save(flush: true)
        redirect(action: "show", id: customer.id)
    }

    def delete(Long id) {
        def customer = Customer.get(id)
        customer.delete(flush: true)
        redirect action: "index"
    }


}
