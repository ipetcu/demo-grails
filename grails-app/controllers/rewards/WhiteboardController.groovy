package rewards

class WhiteboardController {

    def calculationsService

    def index() {}

    def variables() {
        def myTotal = 1.34
        render("Total: " + myTotal)

        render("</br>" + myTotal.class)

        myTotal = myTotal + 1
        render("</br>New Total: " + myTotal + "</br>")

        def firstName = "Mike"
        render("</br>Name: " + firstName)

        render("</br>" + firstName.class)

        firstName = firstName + 1
        render("</br>New Name: " + firstName + "</br>")

        def today = new Date()
        render("</br>Today is: " + today)

        render("</br>" + today.class)

        today = today + 1
        render("</br>New Date: " + today + "</br>")

    }

    def strings() {

        def first = "Mike"
        def last = "Kelly"

        def input = "CEvaScrisMareESTEACUMMIC"

        def fullName = "Ion Pascu"

        def points = 4

        //render "Welcome back " +  " " + first + "." + "You have " + points + " " + "points"

        //render "Hey there $first. You already have $points points"

        //render "Today is ${new Date()}"
        //render "Numele tau complet este $fullName si are ${fullName.size()} caractere"

        render "Scris totul mic ${input.toLowerCase()}"

    }


    def conditions() {
        def firstName = "Mike"
        def totalPoints = 5
        def welcomeMessage = ""

        switch (totalPoints) {
            case 5:
                welcomeMessage = "Welcome back $firstName, this drink is on us"
                break
            case 4:
                welcomeMessage = "Welcome back $firstName, your next drink is free"
                break
            case 2..3:
                welcomeMessage = "Welcome back $firstName, you have $totalPoints points"
                break
            default:
                welcomeMessage = "Welcome $firstName. Thanks for registering"
        }

        render welcomeMessage
    }

    def conditionsParams() {

        def welcomeMessage = calculationsService.welcome(params)

        render welcomeMessage
    }
}
